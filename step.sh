#!/bin/bash
# fail if any commands fails
set -e

# Set default color
envman add --key SLACK_COLOR --value "#3bc3a3"

echo $BITRISEIO_PULL_REQUEST_REPOSITORY_URL
if [[ "$BITRISEIO_PULL_REQUEST_REPOSITORY_URL" == *"bitbucket.org:appstrakt"* ]]; then
      URL="https://bitbucket.org/appstrakt/$BITRISE_APP_TITLE/pull-requests/$BITRISE_PULL_REQUEST"
      echo "PR URL: $URL"
      envman add --key PULL_REQUEST_URL --value $URL
else
      echo "beup"
      envman add --key PULL_REQUEST_URL --value ''
fi

cd "$BITRISE_SOURCE_DIR/bitrise-results"

FAILED=false
NO_LOGS=true

if [ $BITRISE_BUILD_STATUS -ne 0 ]; then
      FAILED=true
fi

WARNINGS=false
ANALYTICS_DATA=""

for i in *.result.log; do
      [ -f "$i" ] || break
      if [ ! -z ${SUMMARY+x} ]; then SUMMARY+="\n"; fi

      STATUS=$(grep STATUS $i | cut -d '=' -f2)
      WORKFLOW=$(grep WORKFLOW $i | cut -d '=' -f2)
      BUILD_URL=$(grep BUILD_URL $i | cut -d '=' -f2)

      if [ ! -z ${ANALYTICS_DATA} ]; then
         ANALYTICS_DATA+="&"
      fi

      ANALYTICS_DATA+="${WORKFLOW}_status="

      # When build succeeds
      if [ $STATUS -eq 0 ]; then
         SUMMARY+="✅"
         ANALYTICS_DATA+="success"
      # When build has a warning
      elif [ $STATUS -eq 2 ]; then
         SUMMARY+="⚠️"
         WARNINGS=true
         ANALYTICS_DATA+="warning"
      # When build fails
      else
         SUMMARY+="❌"
         FAILED=true
         ANALYTICS_DATA+="failed"
      fi

      SUMMARY+=" - <$BUILD_URL|$WORKFLOW>"
      NO_LOGS=false
done

envman add --key ANALYTICS_TRACKING_DATA --value "$ANALYTICS_TRACKING_DATA\n${ANALYTICS_DATA}"

if [ $FAILED = true ]; then
      MESSAGE="🚔🚨 Woop-woop! That's the sound of da P(R)olice! 🚨🚔
You might want to check your PR because our build system detected some issues.
We all want clean and working code right?"
elif [ $WARNINGS = true ]; then
      MESSAGE="🕵️‍♂️ Well well what do we have here. 🕵️‍♂️
An almost perfect PR. You might want to checkout those warnings.
The grass is always greener 🍀 on the perfect PR side right? 🙌"
else
      MESSAGE=":tada: You make me happy! :tada:
That PR passed all the checks.
Now wait for the approval from a colleague and you're good to go! 🚀"
fi

if [ $NO_LOGS = true ]; then
      MESSAGE="Something went wrong with the build. Please check the logs"
      SUMMARY=""
fi

echo $MESSAGE
echo $SUMMARY

envman add --key BUILD_RESULTS_MESSAGE --value "$MESSAGE"
envman add --key BUILD_RESULTS_SUMMARY --value "$SUMMARY"

if [ $FAILED = true ]; then
      echo "Failing the build because some checks failed"
      envman add --key SLACK_COLOR --value "#f0741f"
      exit 1
elif [ $WARNINGS = true ]; then
      envman add --key SLACK_COLOR --value "#FFD136"
else
      envman add --key SLACK_COLOR --value "#3bc3a3"
fi
